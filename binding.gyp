{
    "targets": [
        {
            "target_name": "NodeJulia",
            "include_dirs" : [
			"<!(node -e \"require('nan')\")",
			"./include",
			"/usr/local/include",
			"/usr/local/include/julia"
			],
			"cflags_cc": ["-fPIC", "-DJULIA_ENABLE_THREADING=0", "-D_GLIBCXX_USE_CXX11_ABI=0", "-Wl,--export-dynamic"
				, "-Wl,-rpath,'/usr/lib'"
				, "-Wl,-rpath,'/usr/lib/julia'"],
			"sources": [ "./src/NodeJulia.cc", "./src/pi.cc" ],
			"libraries": ["<!@(pkg-config --libs nodejulia)"]
        }
    ],
}

