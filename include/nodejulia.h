#ifndef NODE_JULIA_H
#define NODE_JULIA_H

#include <nan.h>
#include <julia/julia.h>
#include <julia_plusplus.hpp>

NAN_METHOD(pi);

class NodeJulia : public Nan::ObjectWrap {
  public:
    static NAN_MODULE_INIT(Init);

  private:
    explicit NodeJulia(double value = 0);
    ~NodeJulia();

    static NAN_METHOD(New);
    static NAN_METHOD(PlusOne);
    static Nan::Persistent<v8::Function> constructor;
    double value_;
};

#endif
