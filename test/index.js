var nodeJulia = require('./../build/Release/NodeJulia');
var assert = require('assert');

// Simple unit test to check if pi is computed (for now just type checking)
describe('node-julia native extension', function() {
  
  it('should compute value of pi in Julia', function() {
    assert.equal(typeof nativeExtension.pi(), 'number');
  });
});
