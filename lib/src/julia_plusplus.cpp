#include "julia_plusplus.hpp"
using namespace std;

__attribute__ ((constructor)) bool somain() {
	printf("[JULIA_PLUSPLUS] Loading Julia...\n");
	jl_init();
	return true;
}

EXPORTS double computePi() {
	try {
		jl_eval_string("print(sqrt(2.0))");

	} catch(std::exception& ex) {
		cout<<"[JULIA_PLUSPLUS] Exception: "<<ex.what()<<endl;
		return 0.0;
	}

	return 0.0;
}

