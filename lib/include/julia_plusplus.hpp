#ifndef JULIA_PLUSPLUS_HPP
#define JULIA_PLUSPLUS_HPP

#include <iostream>
#include <julia/julia.h>

#if defined __linux__
#define EXPORTS __attribute__ ((visibility ("default")))
#endif

EXPORTS double computePi();

#endif

