#include "nodejulia.h"

using v8::FunctionTemplate;


NAN_MODULE_INIT(InitAll) {

  jl_init_with_image("/usr/lib/julia", "sys");

  Nan::Set(target, Nan::New("pi").ToLocalChecked(),
    Nan::GetFunction(Nan::New<FunctionTemplate>(pi)).ToLocalChecked());

  NodeJulia::Init(target);
}

NODE_MODULE(NodeJulia, InitAll)
