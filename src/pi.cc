#include "nodejulia.h"

#include <iostream>
using namespace std;

NAN_METHOD(pi) {
    computePi();
    info.GetReturnValue().Set(0.0);
}

Nan::Persistent<v8::Function> NodeJulia::constructor;

NAN_MODULE_INIT(NodeJulia::Init) {
  v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
  tpl->SetClassName(Nan::New("NodeJulia").ToLocalChecked());
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  Nan::SetPrototypeMethod(tpl, "plusOne", PlusOne);

  constructor.Reset(Nan::GetFunction(tpl).ToLocalChecked());
  Nan::Set(target, Nan::New("NodeJulia").ToLocalChecked(), Nan::GetFunction(tpl).ToLocalChecked());
}

NodeJulia::NodeJulia(double value) : value_(value) {
}

NodeJulia::~NodeJulia() {
}

NAN_METHOD(NodeJulia::New) {
  if (info.IsConstructCall()) {
    double value = info[0]->IsUndefined() ? 0 : Nan::To<double>(info[0]).FromJust();
    NodeJulia *obj = new NodeJulia(value);
    obj->Wrap(info.This());
    info.GetReturnValue().Set(info.This());
  } else {
    const int argc = 1;
    v8::Local<v8::Value> argv[argc] = {info[0]};
    v8::Local<v8::Function> cons = Nan::New(constructor);
    info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
  }
}

NAN_METHOD(NodeJulia::PlusOne) {
  NodeJulia* obj = Nan::ObjectWrap::Unwrap<NodeJulia>(info.This());
  obj->value_ += 1;
  info.GetReturnValue().Set(obj->value_);
}

