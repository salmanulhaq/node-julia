# node-julia

A simple NodeJS native module which integrates with Julia through its C API

# Build

There's the native extension and then there's a C++ lib which wraps Julia C API. This additional step is added to seamlessly change function definitions without the need to recompile native module.

## Dependencies
Built and tested on

*    Ubuntu 18.04
*    Julia v1.0.1 built from source ((https://github.com/JuliaLang/julia)[https://github.com/JuliaLang/julia])
*    NodeJS v8.10.0
*    nan 2.11.1
*    gcc/g++ v7.3.0
*    npm v3.5.2
*    node-gyp v3.8.0
*    cmake v3.10.2
*    Mocha 5.2.0

```sh
sudo cp nodejulia.pc /usr/local/lib/pkgconfig
cd lib
mkdir build && cd build
cmake ..
make -j
sudo make install # This will instal julia_plusplus in /usr/include and /usr/local/lib
cd ../..
npm i # Just needed for the first time
npm run configure # This as well
npm run build # Do npm run rebuild to rebuild
mocha # Run tests or alternatively, just node tests/julia
```

...or just run ```build.sh```