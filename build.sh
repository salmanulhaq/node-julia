#!/bin/sh
sudo cp nodejulia.pc /usr/local/lib/pkgconfig
cd lib
mkdir build && cd build
cmake ..
make -j
sudo make install # This will instal julia_plusplus in /usr/include and /usr/local/lib
cd ../..
npm i # Just needed for the first time
npm run configure # This as well
npm run build # Do npm run rebuild to rebuild
mocha # Run tests or alternatively, just node tests/julia

